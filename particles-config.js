particlesJS('particles-js',
{
  "particles": {
    "number": {
      "value": 85,
      "density": {
        "enable": true,
        "value_area": 333.3805622463184
      }
    },
    "color": {
      "value": "#ffffff"
    },
    "shape": {
      "type": "circle",
      "stroke": {
        "width": 0,
        "color": "#000000"
      },
      "polygon": {
        "nb_sides": 6
      },
      "image": {
        "src": "img/github.svg",
        "width": 100,
        "height": 100
      }
    },
    "opacity": {
      "value": 0.5,
      "random": false,
      "anim": {
        "enable": false,
        "speed": 4.645270270270267,
        "opacity_min": 0,
        "sync": false
      }
    },
    "size": {
      "value": 4.16725702807898,
      "random": true,
      "anim": {
        "enable": false,
        "speed": 78.54729729729723,
        "size_min": 4.222972972972969,
        "sync": false
      }
    },
    "line_linked": {
      "enable": true,
      "distance": 166.6902811231592,
      "color": "#ffffff",
      "opacity": 0.4,
      "width": 1.0001416867389552
    },
    "move": {
      "enable": true,
      "speed": 20.002833734779102,
      "direction": "none",
      "random": true,
      "straight": false,
      "out_mode": "bounce",
      "bounce": true,
      "attract": {
        "enable": true,
        "rotateX": 15000.9742354682508,
        "rotateY": 15000.222088423938
      }
    }
  },
  "interactivity": {
    "detect_on": "canvas",
    "events": {
      "onhover": {
        "enable": false,
        "mode": "repulse"
      },
      "onclick": {
        "enable": false,
        "mode": "push"
      },
      "resize": true
    },
    "modes": {
      "grab": {
        "distance": 400,
        "line_linked": {
          "opacity": 1
        }
      },
      "bubble": {
        "distance": 400,
        "size": 40,
        "duration": 2,
        "opacity": 8,
        "speed": 3
      },
      "repulse": {
        "distance": 745.7627118644068,
        "duration": 0.4
      },
      "push": {
        "particles_nb": 4
      },
      "remove": {
        "particles_nb": 2
      }
    }
  },
  "retina_detect": true
});
